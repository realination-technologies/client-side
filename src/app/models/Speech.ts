export class Speech {
  id: number;
  author: string;
  subject: string;
  speech_date: any;
  content: string;
}
