import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';

import {BsDropdownModule, ModalModule, TabsModule} from 'ngx-bootstrap';
import {NgxEditorModule} from 'ngx-editor';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ViewSpeechComponent} from './components/view-speech/view-speech.component';
import {NewSpeechComponent} from './components/new-speech/new-speech.component';
import {SearchSpeechComponent} from './components/search-speech/search-speech.component';
import {DateService} from './services/date.service';
import {NotifierModule} from 'angular-notifier';
import {ShareModalComponent} from './components/share-modal/share-modal.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TagInputModule} from 'ngx-chips';
import {DataTableModule} from 'angular-6-datatable';
import {AuthenticationComponent} from './components/authentication/authentication.component';
import {AuthGuardService} from './guards/auth-guard.service';

import {SpeechComponent} from './components/speech/speech.component';
import {RoutingModule} from './routing.module';
import {SettingsService} from './services/settings.service';
import {TokenInterceptor} from './intercepters/token.interceptor';


@NgModule({
  entryComponents: [
    ShareModalComponent
  ],
  declarations: [
    AppComponent,
    ViewSpeechComponent,
    NewSpeechComponent,
    SearchSpeechComponent,
    ShareModalComponent,
    AuthenticationComponent,
    SpeechComponent
  ],
  imports: [
    RoutingModule,
    TagInputModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    TabsModule.forRoot(),
    NgxEditorModule,
    HttpClientModule,
    DataTableModule,
    NotifierModule.withConfig( {
      position: {

        horizontal: {
          position: 'right',
          distance: 12
        },
        vertical: {
          position: 'top',
        }

      }
    } ),
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
  ],
  providers: [
    AuthGuardService,
    DateService,
    SettingsService,
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
