import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SettingsService} from './settings.service';

@Injectable({
  providedIn: 'root'
})
export class SpeechService {

  constructor(private http: HttpClient, private settingsService: SettingsService) { }

  getSpeeches() {
    return this.http.get(this.settingsService.getApiUrl('speech'));
  }

  addSpeech(speech) {
    return this.http.post(this.settingsService.getApiUrl('speech'), speech);
  }

  shareSpeech(data) {
    return this.http.post(this.settingsService.getApiUrl('share'), data);
  }

  updateSpeech(speech, id) {
    return this.http.put(this.settingsService.getApiUrl('speech/' + id), speech);
  }

  searchSpeech(speech) {
    return this.http.get(this.settingsService.getApiUrl(
      'speech/search?author=' + speech.author + '&subject=' + speech.subject + '&date=' + speech.speechDate));
  }

}
