import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DateService {

  constructor() { }

  getCurrentDate() {
    const today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1;

    const yyyy = today.getFullYear();
    if (dd < 10) {
      // @ts-ignore
      dd = '0' + dd;
    }
    if (mm < 10) {
      // @ts-ignore
      mm = '0' + mm;
    }
    return yyyy + '-' + mm + '-' + dd;
  }
}
