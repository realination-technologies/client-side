import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SettingsService} from './settings.service';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  encryptSecretKey = 'XT7ajDvABCKdPpZ5kalhWpDY7fiEnmVan4UZT47z91n4eCWgEXDGLdwVjmvT';

  constructor(private http: HttpClient, private settingsService: SettingsService) { }

  login(login) {
    return this.http.post(this.settingsService.getApiUrl('auth/login'), login);
  }

  encryptString(data) {
    return CryptoJS.AES.encrypt(JSON.stringify(data), this.encryptSecretKey).toString();
  }

  decryptString(data) {
    try {
      const bytes = CryptoJS.AES.decrypt(data, this.encryptSecretKey);
      if (bytes.toString()) {
        return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      }
      return data;
    } catch (e) {
      console.log(e);
    }
  }
}
