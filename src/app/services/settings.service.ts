import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  private apiUrl = 'http://troll.dentaltrends.com.ph/API/';

  constructor() { }

  getApiUrl(subject) {
    return this.apiUrl + subject;
  }
}
