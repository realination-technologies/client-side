import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {NotifierService} from 'angular-notifier';

@Component({
  selector: 'troll-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.css']
})
export class AuthenticationComponent implements OnInit {

  loginForm = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
  });

  constructor(private authService: AuthService, private router: Router, private notifier: NotifierService) { }

  ngOnInit() {
  }

  login() {
    this.authService.login(this.loginForm.value)
      .subscribe(res => {
        console.log(res);
        // @ts-ignore
        if (res.status !== 'error') {
          this.notifier.notify( 'success', 'Login Successful!' );
          const token = this.authService.encryptString(JSON.stringify(res));
          sessionStorage.setItem('loggedIn', token);
          this.router.navigate(['/']);
        } else {
          this.notifier.notify( 'error', 'Login Failed!' );
        }
      });
  }
}
