import {Component, OnInit} from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap';
import {SpeechService} from '../../services/speech.service';
import {NotifierService} from 'angular-notifier';

@Component({
  selector: 'troll-share-modal',
  templateUrl: './share-modal.component.html',
  styleUrls: ['./share-modal.component.css']
})
export class ShareModalComponent implements OnInit {

  emails = [];
  speech: number;

  constructor(public bsModalRef: BsModalRef, private speechService: SpeechService, private notifier: NotifierService) {}

  ngOnInit() {
  }


  sendEmails() {
    const data = {
      emails: this.emails,
      speech: this.speech
    };

    this.speechService.shareSpeech(data)
      .subscribe(res => {
        console.log(res);
        this.bsModalRef.hide();
        this.notifier.notify( 'success', 'Speech Shared!' );
      });

  }

}
