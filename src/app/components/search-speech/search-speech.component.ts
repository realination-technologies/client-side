import {Component, OnInit} from '@angular/core';
import {SpeechService} from '../../services/speech.service';
import {FormControl, FormGroup} from '@angular/forms';

declare var $: any;
@Component({
  selector: 'troll-search-speech',
  templateUrl: './search-speech.component.html',
  styleUrls: ['./search-speech.component.css']
})
export class SearchSpeechComponent implements OnInit {

  speeches;
  searchForm = new FormGroup({
    author: new FormControl(''),
    subject: new FormControl(''),
    speechDate: new FormControl(''),
  });

  constructor(private speechService: SpeechService) { }

  ngOnInit() {
    this.speechService.getSpeeches()
      .subscribe(res => {
        console.log(res);
        this.speeches = res;
      });
  }

  searchSpeech() {
    $('#loading').show();
    $('#speech-list').hide();
    this.speechService.searchSpeech(this.searchForm.value)
      .subscribe(res => {
        // console.log(res);
        this.speeches = res;
        $('#loading').hide();
        $('#speech-list').show();
      });
  }

}
