import {Component, OnInit} from '@angular/core';
import {SpeechService} from '../../services/speech.service';
import {Speech} from '../../models/Speech';
import {NotifierService} from 'angular-notifier';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {ShareModalComponent} from '../share-modal/share-modal.component';

declare var $: any;

@Component({
  selector: 'troll-view-speech',
  templateUrl: './view-speech.component.html',
  styleUrls: ['./view-speech.component.css']
})
export class ViewSpeechComponent implements OnInit {

  speeches;
  bsModalRef: BsModalRef;
  speechOpened: Speech = new Speech();
  origSpeechOpened = '';

  constructor(private speechService: SpeechService, private notifier: NotifierService, private modalService: BsModalService) { }

  ngOnInit() {
    this.speechService.getSpeeches()
      .subscribe(res => {
        console.log(res);
        this.speeches = res;
      });

  }


  openSpeech(speech) {
    this.origSpeechOpened = JSON.stringify(speech);
    const display = $('#speech-display');
    $(display).slideUp();
    const parent = this;
    setTimeout(
      function() {
        parent.speechOpened = speech;
        $(display).slideDown();
      }, 500);

  }

  discardSpeech() {
    this.speechOpened = JSON.parse(this.origSpeechOpened);
    this.notifier.notify( 'warning', 'Speech Changes Discarded!' );
  }

  shareSpeech() {
    this.bsModalRef = this.modalService.show(ShareModalComponent, {});
    this.bsModalRef.content.speech = this.speechOpened.id;
    console.log(this.speechOpened);
  }

  saveSpeech() {
    this.speechService.updateSpeech(this.speechOpened, this.speechOpened.id)
      .subscribe(res => {
        console.log(res);
        this.notifier.notify( 'success', 'Changes Saved!' );
      });
  }
}
