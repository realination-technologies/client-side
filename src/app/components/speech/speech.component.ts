import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {NotifierService} from 'angular-notifier';
import {SearchSpeechComponent} from '../search-speech/search-speech.component';
import {ViewSpeechComponent} from '../view-speech/view-speech.component';

@Component({
  selector: 'troll-speech',
  templateUrl: './speech.component.html',
  styleUrls: ['./speech.component.css']
})
export class SpeechComponent implements OnInit {
  @ViewChild(SearchSpeechComponent) searchChild: SearchSpeechComponent;
  @ViewChild(ViewSpeechComponent) viewChild: ViewSpeechComponent;

  constructor(private router: Router, private notifier: NotifierService) { }

  ngOnInit() {
  }

  logout() {
    this.notifier.notify( 'warning', 'You have logged out!' );
    sessionStorage.clear();
    this.router.navigate(['/login']);
  }

  reloadSearchSpeech() {
    this.searchChild.ngOnInit();
  }

  reloadViewSpeech() {
    this.viewChild.ngOnInit();
  }
}
