import {Component, OnInit} from '@angular/core';
import {DateService} from '../../services/date.service';
import {SpeechService} from '../../services/speech.service';
import {NotifierService} from 'angular-notifier';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {ShareModalComponent} from '../share-modal/share-modal.component';

@Component({
  selector: 'troll-new-speech',
  templateUrl: './new-speech.component.html',
  styleUrls: ['./new-speech.component.css']
})
export class NewSpeechComponent implements OnInit {

  htmlContent = '';
  author: string;
  speechDate: string;
  subject: string;
  bsModalRef: BsModalRef;
  savedId;

  constructor(private dateService: DateService, private speechService: SpeechService, private notifier: NotifierService,
              private modalService: BsModalService) {}

  ngOnInit() {
    this.speechDate = this.dateService.getCurrentDate();
    console.log(this.speechDate);
  }

  private getSpeech() {
    return {
      content: this.htmlContent,
      author: this.author,
      speech_date: this.speechDate,
      subject: this.subject
    };
  }

  saveSpeech() {
    const speech = this.getSpeech();

  this.speechService.addSpeech(speech)
    .subscribe(res => {
      console.log(res);
      this.clearForm();
      this.savedId = res;
      this.notifier.notify( 'success', 'Speech Saved!' );
    });

  }

  discardSpeech() {
    this.clearForm();
    this.notifier.notify( 'error', 'Speech Discarded!' );
  }

  private clearForm() {
    this.htmlContent = '';
    this.author = '';
    this.speechDate = this.dateService.getCurrentDate();
    this.subject = '';
  }

  saveAndShare() {

    const speech = this.getSpeech();

    const parent = this;
    this.speechService.addSpeech(speech)
      .subscribe(res => {
        console.log(res);
        parent.clearForm();
        parent.notifier.notify( 'success', 'Speech Saved!' );

        parent.bsModalRef = parent.modalService.show(ShareModalComponent, {});
        parent.bsModalRef.content.speech = res;

      });


  }
}
