import {ExtraOptions, RouterModule, Routes} from '@angular/router';
import {SpeechComponent} from './components/speech/speech.component';
import {NgModule} from '@angular/core';
import {AuthenticationComponent} from './components/authentication/authentication.component';
import {AuthGuardService} from './guards/auth-guard.service';

const routes: Routes = [
  { path: '', component: SpeechComponent, canActivate: [AuthGuardService] },
  { path: 'login', component: AuthenticationComponent },
];

const config: ExtraOptions = {
  useHash: true,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
})
export class RoutingModule {
}
